**Aditya:**
- **What are pseudo-classes in CSS?**
  - Pseudo-classes are special keywords that specify a state of an element. They allow you to style elements based on user interaction or document structure. For example, `:hover` changes the style when you hover over an element.

**Akhilesh:**
- **What is the difference between inner join and outer join?**
  - Inner join returns only the rows that have matching values in both tables, while outer join returns all the rows from one table and the matching rows from the other, with unmatched rows filled with NULL values.

**Amruth:**
- **What are transactions in SQL?**
  - Transactions in SQL are a set of operations that are performed as a single unit. They are atomic, consistent, isolated, and durable (ACID properties). Transactions ensure that either all operations are completed successfully, or none of them are.

**Harshit:**
- **What are indexes in SQL and why do we use indexes?**
  - Indexes in SQL are data structures used to quickly locate and access the rows in a table. They improve the speed of data retrieval operations like SELECT queries by reducing the number of disk accesses. Indexes are crucial for efficiently querying large databases.

**Kriti:**
- **What is a self-join in SQL?**
  - A self-join in SQL is when a table is joined with itself. It's like joining two copies of the same table, but with different aliases. Self-joins are useful when you need to compare rows within the same table.


**Manisha:**
- **What is the difference between while and do-while loop?**
  - In a while loop, the condition is checked before the loop body executes. In a do-while loop, the loop body executes at least once before the condition is checked.

**Penchal:**
- **What is an identifier and what are the rules for identifiers?**
  - An identifier is a name given to a variable, function, class, etc., in a program. Rules for identifiers typically include starting with a letter or underscore, consisting of letters, digits, or underscores, and not being a reserved word.

**Ravi:**
- **What is a transfer statement in Python?**
  - There's no specific "transfer statement" in Python. Perhaps you meant "transfer statement" as a generic term referring to statements like `return`, `break`, `continue`, or `pass`, which control the flow of execution in a program.

**Rishab:**
- **What is the difference between git fetch and git pull?**
  - Git fetch retrieves the latest changes from a remote repository without merging them into your local branch, while git pull fetches and merges the changes into your local branch.

**Umang:**
- **What is the difference between pseudo-element and pseudo-class in CSS?**
  - Pseudo-classes select elements based on their state or position in the document, like `:hover` or `:first-child`. Pseudo-elements, on the other hand, style a specific part of an element, such as `::before` or `::after`.

**Vaibhav:**
- **How to join values of two columns into a single column, like combining col1 (first name) and col2 (last name) into a single column?**
  - You can use the concatenation operator (`||` in many SQL dialects) to join values from two columns into a single column. For example, in SQL: `SELECT col1 || ' ' || col2 AS full_name FROM table_name;`